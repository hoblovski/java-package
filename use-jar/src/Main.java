import hoblovski.arith.Arith;
import hoblovski.arith.Add;
import hoblovski.arith.Sub;
import hoblovski.utils.Print;

public class Main {
  Arith a1;
  Arith a2;

  public static void main(String[] args) {
    Main m = new Main();
    m.a1 = new Add(4, 5);
    m.a2 = new Sub(10, 5);
    Print.doPrint(m.a1.getRes()); 
    Print.doPrint(m.a2.getRes()); 
  }
}
