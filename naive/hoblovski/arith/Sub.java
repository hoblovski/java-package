package hoblovski.arith;

public class Sub implements Arith {
  private int a;
  private int b;

  public Sub(int a_, int b_) {
    a = a_;
    b = b_;
  }

  public int getRes() {
    return a - b;
  }
}
