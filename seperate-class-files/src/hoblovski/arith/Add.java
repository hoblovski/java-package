package hoblovski.arith;

public class Add implements Arith {
  private int a;
  private int b;

  public Add(int a_, int b_) {
    a = a_;
    b = b_;
  }

  public int getRes() {
    return a + b;
  }
}
